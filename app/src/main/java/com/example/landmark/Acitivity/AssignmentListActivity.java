package com.example.landmark.Acitivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.APIService.ApiService;
import com.example.landmark.APIService.RetrofitClient;
import com.example.landmark.Adapter.DeleteAdapter;
import com.example.landmark.Adapter.SeperateAssignmentAdapter;
import com.example.landmark.Adapter.Seperate_Filter_Adapter;
import com.example.landmark.Adapter.UpdatedAdapter;
import com.example.landmark.Adapter.WeekendPending_Adapter;
import com.example.landmark.LayoutManager.CustomLinearLayoutManager;
import com.example.landmark.Model.Assignment;
import com.example.landmark.Model.AssignmentData;
import com.example.landmark.Model.Pending;
import com.example.landmark.Model.Updated;
import com.example.landmark.Model.WeekEndData;
import com.example.landmark.Model.WeekendPendingData;
import com.example.landmark.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssignmentListActivity extends AppCompatActivity {

    AutoCompleteTextView weekend;
    ImageView back;
    RecyclerView recyclerView, recyclerView1, recyclerView3;
    LinearLayout home;

    private List<WeekEndData> names = new ArrayList<>();
    WeekendPending_Adapter adapter1;
    DeleteAdapter adapter3;
    UpdatedAdapter updatedAdapter;
    SeperateAssignmentAdapter adapter2;
    Seperate_Filter_Adapter adapter4;
    SharedPreferences share, share1;
    SharedPreferences.Editor editor;
    RadioGroup radioGroup;
    RecyclerView rv;
    RadioButton all, pending, complete, inprogress;
    private List<AssignmentData> names1 = new ArrayList<AssignmentData>();
    Context context;

    @Override
    protected void onRestart() {
        AssignmentListActivity.super.onRestart();
        Rotate1(weekend.getText().toString(), all.getText().toString());
        all.setChecked(true);
        editor.clear();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_assignment);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        home = findViewById(R.id.home_lay);
//        recyclerView = (RecyclerView) findViewById(R.id.rvlist);
        back = findViewById(R.id.back_button);
        weekend = findViewById(R.id.weekend_spinner);
        radioGroup = findViewById(R.id.radiogroup);
        all = findViewById(R.id.all_radio);
        pending = findViewById(R.id.pending);
        complete = findViewById(R.id.complete);
        inprogress = findViewById(R.id.inpogress);
//        recyclerView3 = findViewById(R.id.rvanother);

//        share = getSharedPreferences("Values", MODE_PRIVATE);
//        editor = share.edit();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
//                Intent i = new Intent(AssignmentListActivity.this,HomeScreenActivity.class);
////                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
////                startActivity(i);
//                Activity activity = AssignmentListActivity.this;
//                activity.startActivity(i);
//                activity.overridePendingTransition(R.anim.back_out, R.anim.back);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AssignmentListActivity.this, HomeScreenActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
//                Activity activity = AssignmentListActivity.this;
//                activity.startActivity(i);
//                activity.overridePendingTransition(0,R.anim.bring_home);
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.all_radio:

                        Rotate1(weekend.getText().toString(), all.getText().toString());
                        break;
                    case R.id.pending:

                        Rotate(weekend.getText().toString(), pending.getText().toString());
                        break;
                    case R.id.complete:

                        Rotate(weekend.getText().toString(), complete.getText().toString());
                        break;
                    case R.id.inpogress:

                        Rotate(weekend.getText().toString(), inprogress.getText().toString());
                        break;

                }
            }
        });

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.weekend));


        final String[] selection = new String[1];
        weekend.setAdapter(arrayAdapter);
        weekend.setCursorVisible(false);

        weekend.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                weekend.showDropDown();
                selection[0] = (String) parent.getItemAtPosition(position);
                Rotate1(weekend.getText().toString(), all.getText().toString());
                all.setChecked(true);

            }
        });


        weekend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                weekend.showDropDown();
            }
        });


    }

    private void Rotate1(String week, String status) {

        {
            try {

                share1 = getSharedPreferences("Registration", MODE_PRIVATE);

                String mmobile = share1.getString("mob", "");
                final ProgressDialog loading = ProgressDialog.show(AssignmentListActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
//                loading.setProgressDrawable(getDrawable(loading));

                ApiService apiService = RetrofitClient.getApiService();

                Call<Updated> call = apiService.Getweekendpending(week, status, mmobile);

                call.enqueue(new Callback<Updated>() {
                    @Override
                    public void onResponse(Call<Updated> call, Response<Updated> response) {

                        try {
                            if (response.body().getResult().equals("Success")) {

                                loading.dismiss();
                                rv = findViewById(R.id.rvassigment);

                                share = getSharedPreferences("Values", MODE_PRIVATE);
                                editor = share.edit();
                                editor.clear();
                                editor.putString("Week", weekend.getText().toString());
                                editor.apply();
                                editor.commit();

                                updatedAdapter = new UpdatedAdapter(AssignmentListActivity.this, response.body().getData().getAsc(), response.body().getData().getAsc1());
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(AssignmentListActivity.this);
                                rv.setLayoutManager(layoutManager);
                                rv.setAdapter(updatedAdapter);

                            } else if (response.body().getResult().equals("No Record")) {
                                loading.dismiss();
                                Toast.makeText(AssignmentListActivity.this, "No record found", Toast.LENGTH_SHORT).show();

                            } else {
                                loading.dismiss();
                                Toast.makeText(AssignmentListActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            loading.dismiss();
                            Toast.makeText(AssignmentListActivity.this, "Somethings went wrong", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onFailure(Call<Updated> call, Throwable t) {
                        loading.dismiss();
                        Toast.makeText(AssignmentListActivity.this, "Unable to reach the server,Please try again later", Toast.LENGTH_SHORT).show();
                    }


                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void Rotate(String week, String status) {

        share1 = getSharedPreferences("Registration", MODE_PRIVATE);

        String mmobile = share1.getString("mob", "");
        final ProgressDialog loading = ProgressDialog.show(AssignmentListActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        ApiService apiService = RetrofitClient.getApiService();

        Call<Updated> call = apiService.Getweekendpending(week, status, mmobile);

        call.enqueue(new Callback<Updated>() {
            @Override
            public void onResponse(Call<Updated> call, Response<Updated> response) {

                try {
                    if (response.body().getResult().equals("Success")) {

                        loading.dismiss();
                        if (week.isEmpty()) {
                            weekend.showDropDown();
                        }
                        if (response.body().getData().getAsc().isEmpty()) {

                            loading.dismiss();
                            Toast.makeText(AssignmentListActivity.this, "No record found", Toast.LENGTH_LONG).show();
                            rv = findViewById(R.id.rvassigment);
                            adapter2 = new SeperateAssignmentAdapter(AssignmentListActivity.this, response.body().getData().getAsc());
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(AssignmentListActivity.this);
                            rv.setLayoutManager(layoutManager);
                            rv.setAdapter(adapter2);
                        } else {


                            rv = findViewById(R.id.rvassigment);
                            loading.dismiss();
                            share = getSharedPreferences("Values", MODE_PRIVATE);
                            editor = share.edit();
                            editor.clear();
                            editor.putString("Week", weekend.getText().toString());
                            editor.apply();
                            editor.commit();

                            adapter2 = new SeperateAssignmentAdapter(AssignmentListActivity.this, response.body().getData().getAsc());
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(AssignmentListActivity.this);
                            rv.setLayoutManager(layoutManager);
                            rv.setAdapter(adapter2);
                        }


                    }  else if (response.body().getResult().equalsIgnoreCase("No Record")) {
                        loading.dismiss();
                        Toast.makeText(AssignmentListActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    } else {
                        loading.dismiss();
                        Toast.makeText(AssignmentListActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }


                } catch (
                        Exception e) {
                    loading.dismiss();
                    Toast.makeText(AssignmentListActivity.this, "Please select weekend", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<Updated> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(AssignmentListActivity.this, "Please select weekend", Toast.LENGTH_SHORT).show();
            }


        });


    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(AssignmentListActivity.this, HomeScreenActivity.class);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(i);
        Activity activity = AssignmentListActivity.this;
        activity.startActivity(i);
        activity.overridePendingTransition(R.anim.back_out, R.anim.back);
    }
}
