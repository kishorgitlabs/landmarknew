package com.example.landmark.Acitivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatButton;

import com.example.landmark.APIService.ApiService;
import com.example.landmark.APIService.RetrofitClient;
import com.example.landmark.Model.Classroom;
import com.example.landmark.Model.Description;
import com.example.landmark.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DescriptionActivity extends AppCompatActivity {

    private SharedPreferences share, share1;
    SharedPreferences.Editor editor;
    TextView txt, txt1;
    ImageView but;
    LinearLayout home;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        txt = findViewById(R.id.shortdesc);
        txt1 = findViewById(R.id.logndesc);
        txt1 = findViewById(R.id.logndesc);
        but = findViewById(R.id.back_button);
        home = findViewById(R.id.home_lay);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DescriptionActivity.this, HomeScreenActivity.class));
            }
        });
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DescriptionActivity.super.onBackPressed();
            }
        });

        share = getSharedPreferences("Values", MODE_PRIVATE);
        share1 = getSharedPreferences("Registration", MODE_PRIVATE);

        String week = share.getString("Week", "");
        String assign = getIntent().getStringExtra("HHHH");
        String mmobile = share1.getString("mob", "");

        final ProgressDialog loading = ProgressDialog.show(DescriptionActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        ApiService apiService = RetrofitClient.getApiService();

        Call<Description> call = apiService.Getdescription(assign, week, mmobile);

        call.enqueue(new Callback<Description>() {
            @Override
            public void onResponse(Call<Description> call, Response<Description> response) {

                try {
                    if (response.body().getResult().equals("Success")) {
                        loading.dismiss();
//                        Toast.makeText(DescriptionActivity.this, "fgdgfdgfdgdfgr", Toast.LENGTH_SHORT).show();
                        txt.setText(Html.fromHtml(response.body().getData().getShortdesc()));
                        txt1.setText(Html.fromHtml(response.body().getData().getLongdesc()));
//                        share = getSharedPreferences("Desc",MODE_PRIVATE);
//                        editor = share.edit();
////                        editor.clear();
//                        editor.putString("ShortDesc",response.body().getData().getShortdesc());
//                        editor.apply();
//                        editor.commit();

                    } else if (response.body().getResult().equals("No Record")) {
                        loading.dismiss();
                        Toast.makeText(DescriptionActivity.this, "No records found", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    loading.dismiss();
                    Toast.makeText(DescriptionActivity.this, "Somethingfdgfgdggd went wrong", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Description> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(DescriptionActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }


        });
    }
}
