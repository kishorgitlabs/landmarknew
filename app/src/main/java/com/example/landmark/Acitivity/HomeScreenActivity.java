package com.example.landmark.Acitivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.example.landmark.APIService.ApiService;
import com.example.landmark.APIService.RetrofitClient;
import com.example.landmark.Fragment.ChatFragment;
import com.example.landmark.Fragment.DashboardFragment;
import com.example.landmark.Model.Forgot;
import com.example.landmark.R;
import com.fivemin.chief.nonetworklibrary.networkBroadcast.NoNet;
import com.google.android.material.navigation.NavigationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeScreenActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private FrameLayout frameLayout;
    private NavigationView navigationView;
    private SwitchCompat darkModeSwitch;



    @Override
    protected void onRestart() {
        closeDrawer();
        deSelectCheckedState();
        super.onRestart();

    }

//    @Override
//    protected void onResume() {
//        mNoNet.RegisterNoNet();
//        super.onResume();
//    }
//
//    @Override
//    protected void onPause() {
//        mNoNet.unRegisterNoNet();
//        super.onPause();
//    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            return false;
        }
        return true;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        if (isOnline()) {
            // Do you Stuff
        } else {
            try {
                new AlertDialog.Builder(HomeScreenActivity.this)
                        .setTitle("Error")
                        .setMessage("Check your internet and try again...")
                        .setCancelable(false)
                        .setIcon(R.drawable.warning)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();

                            }
                        }).show();
            } catch (Exception e) {
//                Log.d(SyncStateContract.Constants.TAG, "Show Dialog: " + e.getMessage());
            }
        }


        initializeViews();
        toggleDrawer();

        initializeDefaultFragment(savedInstanceState, 0);

    }

    private void initializeDefaultFragment(Bundle savedInstanceState, int i) {
        if (savedInstanceState == null) {
            MenuItem menuItem = navigationView.getMenu().getItem(i).setChecked(true);
            onNavigationItemSelected(menuItem);
        }
    }

    public boolean onNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_assisting_id:
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_id, new MessageFragment())
//                        .commit();
                startActivity(new Intent(HomeScreenActivity.this, ClassroomActivity.class));
                closeDrawer();
//
                break;
            case R.id.nav_reminder_id:
                forgetPass();
                closeDrawer();
                break;

            case R.id.dashboard:
                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_id, new DashboardFragment()).commit();
                closeDrawer();
//                startActivity(new Intent(HomeScreenActivity.this,CompleteAssignment.class));
                break;

            case R.id.assignment_list:
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_id, new DashboardFragment()).commit();

                startActivity(new Intent(HomeScreenActivity.this, AssignmentListActivity.class));
                closeDrawer();
                break;
//            case R.id.nav_notebooks_id:
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_id,new NotebooksFragment())
//                        .commit();
//                closeDrawer();
//                break;
//            case R.id.nav_send_id:
//                Toast.makeText(this, "Send Pressed", Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.nav_share_id:
//                Toast.makeText(this, "Share Pressed", Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.nav_photos_id:
//                Toast.makeText(this, "Photos Pressed", Toast.LENGTH_SHORT).show();
//                break;
            case R.id.nav_trash_id:

                new AlertDialog.Builder(this)
                        .setIcon(R.drawable.warning)
                        .setTitle("Confirm")
                        .setMessage("Are you sure you want to logout?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreferences preferences = getSharedPreferences("Registration", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.apply();
                                finish();
                                startActivity(new Intent(HomeScreenActivity.this, LoginActivity.class));
                                Toast.makeText(HomeScreenActivity.this, "Successfully Loged out", Toast.LENGTH_SHORT).show();
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
                break;
//            case R.id.nav_exit_id:
//                getSupportFragmentManager().beginTransaction().replace(R.id.framelayout_id, new SettingsFragment())
//                        .commit();
//                deSelectCheckedState();
//                closeDrawer();
//                break;
        }
        return true;
    }

    private void forgetPass() {
        try {
            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(HomeScreenActivity.this).create();

            LayoutInflater inflater = ((Activity) HomeScreenActivity.this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.change_password_layout, null);
            alertDialog.setView(dialogView);
            ImageView close = dialogView.findViewById(R.id.close);
            EditText enterMobile = dialogView.findViewById(R.id.new_password);
            TextView update = dialogView.findViewById(R.id.updated_password);
            Button sent = dialogView.findViewById(R.id.change_password_btn);
//            sent.setText("Sent");
//            enterMobile.setHint("Enter Mobile No or Email");

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            sent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(sent.getWindowToken(), 0);

                    String EnterMobile = enterMobile.getText().toString();
                    if (EnterMobile.equals("")) {
                        enterMobile.setError("Enter Mobile Number");
                    } else {
                        final ProgressDialog loading = ProgressDialog.show(HomeScreenActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                        ApiService service = RetrofitClient.getApiService();

                        Call<Forgot> call = service.ForgetPassword(EnterMobile);

                        call.enqueue(new Callback<Forgot>() {
                            @Override
                            public void onResponse(Call<Forgot> call, Response<Forgot> response) {
                                loading.dismiss();
                                try {
                                    if (response.body().getResult().equals("Success")) {

                                        android.app.AlertDialog alertDialog1 = new android.app.AlertDialog.Builder(HomeScreenActivity.this).create();

                                        LayoutInflater inflater = ((Activity) HomeScreenActivity.this).getLayoutInflater();
                                        View dialogView = inflater.inflate(R.layout.password_alert, null);
                                        alertDialog1.setView(dialogView);
                                        ImageView close = dialogView.findViewById(R.id.close);
                                        AppCompatButton dis = dialogView.findViewById(R.id.dismiss);
                                        Window window = alertDialog1.getWindow();
                                        window.setLayout(600, 600);
                                        alertDialog1.show();
                                        alertDialog.dismiss();

                                        dis.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alertDialog1.dismiss();
                                            }
                                        });

                                        close.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alertDialog1.dismiss();
                                            }
                                        });
//                                    enterMobile.setVisibility(View.GONE);
//                                    update.setVisibility(View.VISIBLE);
//                                    update.setText("Password Sent");
//                                    sent.setVisibility(View.GONE);
                                    } else if (response.body().getResult().equals("Error")) {

                                        Toast.makeText(HomeScreenActivity.this, "Please enter registerd Mobile No or Email", Toast.LENGTH_LONG).show();
//                                    enterMobile.setVisibility(View.GONE);
//                                    update.setVisibility(View.VISIBLE);
//                                    update.setText("Please Try Again Later");
//                                    sent.setVisibility(View.GONE);
                                    } else {
                                        Toast.makeText(HomeScreenActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
//                                    enterMobile.setVisibility(View.GONE);
//                                    update.setVisibility(View.VISIBLE);
//                                    update.setText("Please Try Again Later");
//                                    sent.setVisibility(View.GONE);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(HomeScreenActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<Forgot> call, Throwable t) {
                                loading.dismiss();
//                                enterMobile.setVisibility(View.GONE);
//                                update.setVisibility(View.VISIBLE);
////                                update.setText(getString(R.string.server_error));
//                                sent.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            });


            Window window = alertDialog.getWindow();
            window.setLayout(600, 600);
            alertDialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setDarkModeSwitchListener() {
        darkModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    Toast.makeText(HomeScreenActivity.this, "Dark Mode Turn Off", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(HomeScreenActivity.this, "Dark Mode Turn On", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void closeDrawer() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void deSelectCheckedState() {
        int noOfItems = navigationView.getMenu().size();
        for (int i = 0; i < noOfItems; i++) {
            navigationView.getMenu().getItem(0).setChecked(true);
        }
    }

    private void toggleDrawer() {
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            drawerToggle.getDrawerArrowDrawable().setColor(getColor(R.color.white));
        }
        drawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        //Checks if the navigation drawer is open -- If so, close it
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        // If drawer is already close -- Do not override original functionality
        else {
            new AlertDialog.Builder(this)
                    .setIcon(R.drawable.warning)
                    .setTitle("Exit")
                    .setMessage("Are you sure you want to close?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finishAffinity();
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void initializeViews() {

        toolbar = findViewById(R.id.toolbar_id);
        toolbar.setTitle("Dashboard");


//        toolbar.setTitleTextColor("#148399");
//        toolbar.setTitleTextColor(Integer.parseInt("#148399"));
//        toolbar.setLogo(R.drawable.landmark_logo);
//        toolbar.setNavigationIcon(R.drawable.navigation_icon);


        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.drawer_layout_id);
        frameLayout = findViewById(R.id.framelayout_id);
        navigationView = findViewById(R.id.navigationview_id);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        navigationView.setItemIconTintList(null);
//        darkModeSwitch = (SwitchCompat)navigationView.getMenu().findItem(R.id.nav_darkmode_id).getActionView();
    }

}
