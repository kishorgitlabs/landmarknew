package com.example.landmark.Acitivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.APIService.ApiService;
import com.example.landmark.APIService.RetrofitClient;
import com.example.landmark.Adapter.ClassroomAdapter;
import com.example.landmark.Model.ClassData;
import com.example.landmark.Model.Classroom;
import com.example.landmark.Model.CoachLocation;
import com.example.landmark.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClassroomActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ClassroomAdapter adapter;
    ImageView back;
    LinearLayout home;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classroom);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        back = findViewById(R.id.back_button);
        home = findViewById(R.id.home_lay);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ClassroomActivity.this, HomeScreenActivity.class));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClassroomActivity.super.onBackPressed();
            }
        });

//        recyclerView = findViewById(R.id.rvclass);

        final ProgressDialog loading = ProgressDialog.show(ClassroomActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        ApiService apiService = RetrofitClient.getApiService();

        Call<Classroom> call = apiService.CLASS_DATA_CALL();

        call.enqueue(new Callback<Classroom>() {
            @Override
            public void onResponse(Call<Classroom> call, Response<Classroom> response) {

                try {
                    if (response.body().getResult().equals("Success")) {
                        loading.dismiss();
                        generateEmployeeList(response.body().getData());
                    } else {
                        loading.dismiss();
                        Toast.makeText(ClassroomActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    loading.dismiss();
                    Toast.makeText(ClassroomActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Classroom> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(ClassroomActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }


        });
    }

    private void generateEmployeeList(List<ClassData> data) {

        recyclerView = (RecyclerView) findViewById(R.id.rvclass);

        adapter = new ClassroomAdapter(data);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ClassroomActivity.this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);
    }
}
