package com.example.landmark.Acitivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatButton;

import com.example.landmark.APIService.ApiService;
import com.example.landmark.APIService.RetrofitClient;
import com.example.landmark.Model.Description;
import com.example.landmark.Model.SourceList;
import com.example.landmark.Model.UpdateAssignment;
import com.example.landmark.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity {

    AutoCompleteTextView spinner, why_spinner;
    private SharedPreferences share, share1, share2, share3;
    ImageView back;
    TextView pending_text, inprogress_text, short_;
    LinearLayout complete, home_lay, reason_lay;
    AppCompatButton pending, inprogress, completed;
    EditText datepicker, select_date, completed_text;
    DatePickerDialog pickerDialog;
    String dat;
    private ArrayList<String> sourcelist = new ArrayList<String>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        spinner = findViewById(R.id.status_spinner);
        why_spinner = findViewById(R.id.why_spinner);
        back = findViewById(R.id.back_bu);
        pending_text = findViewById(R.id.pending_text);
        complete = findViewById(R.id.complete_layout);
        inprogress_text = findViewById(R.id.inprogress_text);
        pending = findViewById(R.id.pending_status);
        inprogress = findViewById(R.id.inprogress_status);
        completed = findViewById(R.id.completed_status);
        datepicker = findViewById(R.id.select_date);
        completed_text = findViewById(R.id.completed_text1);
        short_ = findViewById(R.id.short_desc);
        home_lay = findViewById(R.id.home_lay);
        reason_lay = findViewById(R.id.reason_lay);

        reason_lay.setVisibility(View.GONE);

//        String status = getIntent().getStringExtra("sta");
//
//        if(status.equalsIgnoreCase("N/A")){
//            reason_lay.setVisibility(View.GONE);
//        }else {
//            reason_lay.setVisibility(View.VISIBLE);
//        }

        Source();

        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(UpdateActivity.this, HomeScreenActivity.class);
                Activity activity = (Activity) getApplicationContext();
                activity.startActivity(i);
                activity.overridePendingTransition(R.anim.left_out, R.anim.left_in);
            }
        });

//        String assign = getIntent().getStringExtra("value");
        String assign1 = getIntent().getStringExtra("HHHH");


        share = getSharedPreferences("Values", MODE_PRIVATE);
        share2 = getSharedPreferences("Registration", MODE_PRIVATE);
        share3 = getSharedPreferences("Adap", MODE_PRIVATE);

        String mmobile = share2.getString("mob", "");
        String wee = share.getString("Week", "");
//        String as = share3.getString("Ok_200", "");

        final ProgressDialog loading = ProgressDialog.show(UpdateActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        ApiService apiService = RetrofitClient.getApiService();

        Call<Description> call = apiService.Getdescription(assign1, wee, mmobile);

        call.enqueue(new Callback<Description>() {
            @Override
            public void onResponse(Call<Description> call, Response<Description> response) {

                try {
                    if (response.body().getResult().equals("Success")) {
                        loading.dismiss();
                        short_.setText(Html.fromHtml(response.body().getData().getShortdesc().toString()));

                    } else if (response.body().getResult().equals("Error")) {
                        loading.dismiss();
                        Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    } else {
                        loading.dismiss();
                        Toast.makeText(UpdateActivity.this, "Out of condition", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    loading.dismiss();
                    Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Description> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(UpdateActivity.this, "Unable to reach server, Please try again later", Toast.LENGTH_SHORT).show();
            }


        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateActivity.super.onBackPressed();
            }
        });


//        final ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<>(
//                this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.why));
//
//        final String[] selection1 = new String[1];
//        why_spinner.setAdapter(arrayAdapter1);
//        why_spinner.setCursorVisible(false);
//
//        why_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                why_spinner.showDropDown();
//                selection1[0] = (String) parent.getItemAtPosition(position);
//            }
//        });


        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.status));


        final String[] selection = new String[1];
        spinner.setAdapter(arrayAdapter);
        spinner.setCursorVisible(false);
        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                spinner.showDropDown();
                selection[0] = (String) parent.getItemAtPosition(position);

                if (id == 0) {

                    String sta = getIntent().getStringExtra("VVVV");
                    if (sta.equals("N/A")) {
                        reason_lay.setVisibility(View.GONE);
                    } else {
                        reason_lay.setVisibility(View.VISIBLE);
                    }
                    pending_text.setVisibility(View.VISIBLE);
                    inprogress_text.setVisibility(View.GONE);
                    complete.setVisibility(View.GONE);
                    pending.setVisibility(View.VISIBLE);
                    inprogress.setVisibility(View.GONE);
                    completed.setVisibility(View.GONE);
                    datepicker.setVisibility(View.VISIBLE);


                    datepicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final GregorianCalendar cldr = (GregorianCalendar) GregorianCalendar.getInstance();
                            int day = cldr.get(Calendar.DAY_OF_MONTH);
                            int month = cldr.get(Calendar.MONTH);
                            int year = cldr.get(Calendar.YEAR);

                            // date picker dialog
                            pickerDialog = new DatePickerDialog(UpdateActivity.this, new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    datepicker.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                    dat = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;
                                }
                            }, year, month, day);

                            pickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                            pickerDialog.show();
                        }
                    });

                    pending.setClickable(false);

                    pending.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (sta.equals("N/A")) {
                                if (datepicker.getText().toString().equals("")) {
                                    Toast.makeText(UpdateActivity.this, "Choose new Date", Toast.LENGTH_SHORT).show();
                                } else {
                                    {

                                        share = getSharedPreferences("Values", MODE_PRIVATE);
                                        share1 = getSharedPreferences("Registration", MODE_PRIVATE);

                                        String s = share.getString("Week", "");
                                        String m = getIntent().getStringExtra("HHHH");
                                        String n = spinner.getText().toString();
                                        String z = dat;
                                        String o = "";
                                        String p = share1.getString("mob", "");
                                        String q = why_spinner.getText().toString();

                                        final ProgressDialog loading = ProgressDialog.show(UpdateActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                                        ApiService apiService = RetrofitClient.getApiService();

                                        Call<UpdateAssignment> call = apiService.GetUpdateAssign(s, m, n, z, p, o, q);

                                        call.enqueue(new Callback<UpdateAssignment>() {
                                            @Override
                                            public void onResponse(Call<UpdateAssignment> call, Response<UpdateAssignment> response) {

                                                try {
                                                    if (response.body().getResult().equals("Success")) {
                                                        loading.dismiss();
//                                                Toast.makeText(UpdateActivity.this, "Something wfghhdghgfhfhchent wrong", Toast.LENGTH_SHORT).show();
                                                        UpdateActivity.super.onBackPressed();
                                                    } else if (response.body().getResult().equals("Error")) {
//                                                loading.dismiss();
                                                        Toast.makeText(UpdateActivity.this, "Something went wrong, Please try again later", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                    }


                                                } catch (Exception e) {
//                                            loading.dismiss();
                                                    Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                    e.printStackTrace();
                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<UpdateAssignment> call, Throwable t) {
//                                        loading.dismiss();
                                                Toast.makeText(UpdateActivity.this, "Unable to reach the server,Please try again later", Toast.LENGTH_SHORT).show();
                                            }


                                        });

                                    }
                                }
                            } else if (!sta.equals(null)) {
                                if (datepicker.getText().toString().equals("")) {
                                    Toast.makeText(UpdateActivity.this, "Choose new Date", Toast.LENGTH_SHORT).show();
                                } else if (why_spinner.getText().toString().equals("")) {
                                    Toast.makeText(UpdateActivity.this, "Select reason", Toast.LENGTH_SHORT).show();
                                } else {

                                    share = getSharedPreferences("Values", MODE_PRIVATE);
                                    share1 = getSharedPreferences("Registration", MODE_PRIVATE);

                                    String weekend = share.getString("Week", "");
                                    String assignment = getIntent().getStringExtra("HHHH");
                                    String status = spinner.getText().toString();
                                    String completiondate = dat;
                                    String mobileno = share1.getString("mob", "");
                                    String discover = "";
                                    String reason = why_spinner.getText().toString();

                                    final ProgressDialog loading = ProgressDialog.show(UpdateActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                                    ApiService apiService = RetrofitClient.getApiService();

                                    Call<UpdateAssignment> call = apiService.GetUpdateAssign(weekend, assignment, status, completiondate, mobileno, discover, reason);

                                    call.enqueue(new Callback<UpdateAssignment>() {
                                        @Override
                                        public void onResponse(Call<UpdateAssignment> call, Response<UpdateAssignment> response) {

                                            try {
                                                if (response.body().getResult().equals("Success")) {
                                                    loading.dismiss();
//                                                Toast.makeText(UpdateActivity.this, "Something wfghhdghgfhfhchent wrong", Toast.LENGTH_SHORT).show();
                                                    UpdateActivity.super.onBackPressed();
                                                } else if (response.body().getResult().equals("Error")) {
//                                                loading.dismiss();
                                                    Toast.makeText(UpdateActivity.this, "Something went wrong,Please try again later", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                }


                                            } catch (Exception e) {
//                                            loading.dismiss();
                                                Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                e.printStackTrace();
                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<UpdateAssignment> call, Throwable t) {
//                                        loading.dismiss();
                                            Toast.makeText(UpdateActivity.this, "Unable to reach server,Please try again later", Toast.LENGTH_SHORT).show();
                                        }


                                    });

                                }
                            }

                        }
                    });
                } else if (id == 1) {

                    String sta = getIntent().getStringExtra("VVVV");
                    if (sta.equals("N/A")) {
                        reason_lay.setVisibility(View.GONE);
                    } else {
                        reason_lay.setVisibility(View.VISIBLE);
                    }

                    inprogress_text.setVisibility(View.VISIBLE);
                    pending_text.setVisibility(View.GONE);
                    complete.setVisibility(View.GONE);
                    inprogress.setVisibility(View.VISIBLE);
                    pending.setVisibility(View.GONE);
                    completed.setVisibility(View.GONE);
                    datepicker.setVisibility(View.VISIBLE);
                    datepicker.setText("");
                    reason_lay.setVisibility(View.VISIBLE);

                    datepicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final GregorianCalendar cldr = (GregorianCalendar) GregorianCalendar.getInstance();
                            int day = cldr.get(Calendar.DAY_OF_MONTH);
                            int month = cldr.get(Calendar.MONTH);
                            int year = cldr.get(Calendar.YEAR);

                            // date picker dialog
                            pickerDialog = new DatePickerDialog(UpdateActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                            datepicker.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                            dat = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;

                                        }
                                    }, year, month, day);
//                            pickerDialog.getDatePicker().setSpinnersShown(false);
//                            pickerDialog.getDatePicker().setCalendarViewShown(true);
                            pickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                            pickerDialog.show();
                        }
                    });


                    inprogress.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (sta.equals("N/A")) {
                                if (datepicker.getText().toString().equals("")) {
                                    Toast.makeText(UpdateActivity.this, "Choose new Date", Toast.LENGTH_SHORT).show();
                                } else {
                                    {

                                        share = getSharedPreferences("Values", MODE_PRIVATE);
                                        share1 = getSharedPreferences("Registration", MODE_PRIVATE);

                                        String s = share.getString("Week", "");
                                        String m = getIntent().getStringExtra("HHHH");
                                        String n = spinner.getText().toString();
                                        String z = dat;
                                        String o = "";
                                        String p = share1.getString("mob", "");
                                        String q = why_spinner.getText().toString();

                                        final ProgressDialog loading = ProgressDialog.show(UpdateActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                                        ApiService apiService = RetrofitClient.getApiService();

                                        Call<UpdateAssignment> call = apiService.GetUpdateAssign(s, m, n, z, p, o, q);

                                        call.enqueue(new Callback<UpdateAssignment>() {
                                            @Override
                                            public void onResponse(Call<UpdateAssignment> call, Response<UpdateAssignment> response) {

                                                try {
                                                    if (response.body().getResult().equals("Success")) {
                                                        loading.dismiss();
//                                                Toast.makeText(UpdateActivity.this, "Something wfghhdghgfhfhchent wrong", Toast.LENGTH_SHORT).show();
                                                        UpdateActivity.super.onBackPressed();
                                                    } else if (response.body().getResult().equals("Error")) {
//                                                loading.dismiss();
                                                        Toast.makeText(UpdateActivity.this, "Something went wrong,Please try again later", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                    }


                                                } catch (Exception e) {
//                                            loading.dismiss();
                                                    Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                    e.printStackTrace();
                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<UpdateAssignment> call, Throwable t) {
//                                        loading.dismiss();
                                                Toast.makeText(UpdateActivity.this, "Unable to reach the server,Please try again later", Toast.LENGTH_SHORT).show();
                                            }


                                        });

                                    }
                                }
                            } else if (!sta.equals(null)) {
                                if (datepicker.getText().toString().equals("")) {
                                    Toast.makeText(UpdateActivity.this, "Choose new Date", Toast.LENGTH_SHORT).show();
                                } else if (why_spinner.getText().toString().equals("")) {
                                    Toast.makeText(UpdateActivity.this, "Select reason", Toast.LENGTH_SHORT).show();
                                } else {

                                    share = getSharedPreferences("Values", MODE_PRIVATE);
                                    share1 = getSharedPreferences("Registration", MODE_PRIVATE);

                                    String weekend = share.getString("Week", "");
                                    String assignment = getIntent().getStringExtra("HHHH");
                                    String status = spinner.getText().toString();
                                    String completiondate = dat;
                                    String mobileno = share1.getString("mob", "");
                                    String discover = "";
                                    String reason = why_spinner.getText().toString();

                                    final ProgressDialog loading = ProgressDialog.show(UpdateActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                                    ApiService apiService = RetrofitClient.getApiService();

                                    Call<UpdateAssignment> call = apiService.GetUpdateAssign(weekend, assignment, status, completiondate, mobileno, discover, reason);

                                    call.enqueue(new Callback<UpdateAssignment>() {
                                        @Override
                                        public void onResponse(Call<UpdateAssignment> call, Response<UpdateAssignment> response) {

                                            try {
                                                if (response.body().getResult().equals("Success")) {
                                                    loading.dismiss();
//                                                Toast.makeText(UpdateActivity.this, "Something wfghhdghgfhfhchent wrong", Toast.LENGTH_SHORT).show();
                                                    UpdateActivity.super.onBackPressed();
                                                } else if (response.body().getResult().equals("Error")) {
//                                                loading.dismiss();
                                                    Toast.makeText(UpdateActivity.this, "Something went wrong,Please try again later", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                }


                                            } catch (Exception e) {
//                                            loading.dismiss();
                                                Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                                e.printStackTrace();
                                            }

                                        }

                                        @Override
                                        public void onFailure(Call<UpdateAssignment> call, Throwable t) {
//                                        loading.dismiss();
                                            Toast.makeText(UpdateActivity.this, "Unable to reach server,Please try again later", Toast.LENGTH_SHORT).show();
                                        }


                                    });

                                }
                            }

                        }
                    });

                } else if (id == 2) {
                    complete.setVisibility(View.VISIBLE);
                    inprogress_text.setVisibility(View.GONE);
                    pending_text.setVisibility(View.GONE);
                    completed.setVisibility(View.VISIBLE);
                    pending.setVisibility(View.GONE);
                    inprogress.setVisibility(View.GONE);
                    datepicker.setVisibility(View.GONE);
                    reason_lay.setVisibility(View.GONE);

                    datepicker.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final GregorianCalendar cldr = (GregorianCalendar) GregorianCalendar.getInstance();
                            int day = cldr.get(Calendar.DAY_OF_MONTH);
                            int month = cldr.get(Calendar.MONTH);
                            int year = cldr.get(Calendar.YEAR);

                            // date picker dialog
                            pickerDialog = new DatePickerDialog(UpdateActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

//                                          datepicker.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                            dat = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;

                                        }
                                    }, year, month, day);
//                            pickerDialog.getDatePicker().setSpinnersShown(false);
//                            pickerDialog.getDatePicker().setCalendarViewShown(true);
                            pickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                            pickerDialog.show();
                        }
                    });


                    Date c = Calendar.getInstance().getTime();
                    System.out.println("Current time => " + c);

                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                    String formattedDate = df.format(c);

                    completed.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (completed_text.getText().toString().equals("")) {
                                Toast.makeText(UpdateActivity.this, "Enter something", Toast.LENGTH_SHORT).show();
                            } else {

                                share = getSharedPreferences("Values", MODE_PRIVATE);
                                share1 = getSharedPreferences("Registration", MODE_PRIVATE);

                                String s = share.getString("Week", "");
                                String m = getIntent().getStringExtra("HHHH");
                                String n = spinner.getText().toString();
                                String z = formattedDate;
                                String o = completed_text.getText().toString();
                                String p = share1.getString("mob", "");
                                String q = "";

                                final ProgressDialog loading = ProgressDialog.show(UpdateActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                                ApiService apiService = RetrofitClient.getApiService();

                                Call<UpdateAssignment> call = apiService.GetUpdateAssign(s, m, n, z, p, o, q);

                                call.enqueue(new Callback<UpdateAssignment>() {
                                    @Override
                                    public void onResponse(Call<UpdateAssignment> call, Response<UpdateAssignment> response) {

                                        try {
                                            if (response.body().getResult().equals("Success")) {
                                                loading.dismiss();
//                                                Toast.makeText(UpdateActivity.this, "Something wfghhdghgfhfhchent wrong", Toast.LENGTH_SHORT).show();
                                                UpdateActivity.super.onBackPressed();
                                            } else if (response.body().getResult().equals("Error")) {
                                                loading.dismiss();
                                                UpdateActivity.super.onBackPressed();
                                                Toast.makeText(UpdateActivity.this, "Something went wrong,Please try again later", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                            }


                                        } catch (Exception e) {
                                            loading.dismiss();
//                                            Toast.makeText(UpdateActivity.this, "Somethgfdggfding went wrong", Toast.LENGTH_SHORT).show();
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<UpdateAssignment> call, Throwable t) {
                                        loading.dismiss();
                                        Toast.makeText(UpdateActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                    }


                                });
//                                UpdateActivity.super.onBackPressed();
//                                Toast.makeText(UpdateActivity.this, "Successfully updated", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });


        spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                spinner.showDropDown();
            }
        });

        why_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                why_spinner.showDropDown();
            }
        });


    }

    private void Source() {

        final ProgressDialog dialog = ProgressDialog.show(UpdateActivity.this, getResources().getString(R.string.app_name), "Loading Batch...", false, false);
        ApiService apiService = RetrofitClient.getApiService();
        Call<SourceList> call = apiService.Getsource();
        call.enqueue(new Callback<SourceList>() {
            @Override
            public void onResponse(Call<SourceList> call, Response<SourceList> response) {
                if (response.body().getResult().equals("Success")) {

                    try {
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            sourcelist.add(response.body().getData().get(i).getSource());
                        }

//                        batchlist.add(response.body().getData().toString());

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(UpdateActivity.this, android.R.layout.simple_dropdown_item_1line, sourcelist);
                        why_spinner.setAdapter(spinnerArrayAdapter);
                        dialog.dismiss();
                        why_spinner.setCursorVisible(false);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (response.body().getResult().equals("No Record")) {
                    dialog.dismiss();
                    Toast.makeText(UpdateActivity.this, "No Batch List", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SourceList> call, Throwable t) {
                Toast.makeText(UpdateActivity.this, "Something went wrong,Please try again later", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void updateLabel() {
//
//        String myFormat = "MM/dd/yy"; //In which you need put here
//        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
//
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }
}

