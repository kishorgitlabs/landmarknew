package com.example.landmark.Acitivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.APIService.ApiService;
import com.example.landmark.APIService.RetrofitClient;
import com.example.landmark.Adapter.ClassroomAdapter;
import com.example.landmark.Adapter.MoreAdapter;
import com.example.landmark.Model.Description;
import com.example.landmark.Model.Discover;
import com.example.landmark.Model.MoreList;
import com.example.landmark.Model.MoreListData;
import com.example.landmark.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreActivity extends AppCompatActivity {

    SharedPreferences share, share1;
    SharedPreferences.Editor editor;
    TextView short_des, history, discover;
    List<MoreListData> listData;
    RecyclerView recyclerView;
    MoreAdapter adapter;
    ImageView back;
    LinearLayout home;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_more);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        short_des = findViewById(R.id.short_desc);
        back = findViewById(R.id.back_button);
        home = findViewById(R.id.home_lay);
        discover = findViewById(R.id.discover);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MoreActivity.this, HomeScreenActivity.class));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoreActivity.super.onBackPressed();
            }
        });

        String assg = getIntent().getStringExtra("HHHH");
        String sta = getIntent().getStringExtra("IIII");
        share = getSharedPreferences("Values", MODE_PRIVATE);
        String wee = share.getString("Week", "");

        share1 = getSharedPreferences("Registration", MODE_PRIVATE);
        String mob = share1.getString("mob", "");

        History(mob, assg);
        ShortDesc(assg, wee, mob);
        Discover(mob, assg, sta);

    }

    private void Discover(String mob, String assg, String sta) {

        final ProgressDialog loading = ProgressDialog.show(MoreActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        ApiService apiService = RetrofitClient.getApiService();

        Call<Discover> call = apiService.GetDiscover(mob, assg, sta);

        call.enqueue(new Callback<Discover>() {
            @Override
            public void onResponse(Call<Discover> call, Response<Discover> response) {

                try {
                    if (response.body().getResult().equals("Success")) {

                        loading.dismiss();
//                        discover.setText(response.body().getData().toString());
                        discover.setText(Html.fromHtml(response.body().getData().toString()));

//                        recyclerView = (RecyclerView) findViewById(R.id.more_rv);
//                        adapter = new MoreAdapter(response.body().getData());
//                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MoreActivity.this);
//                        recyclerView.setLayoutManager(layoutManager);
//                        recyclerView.setAdapter(adapter);

                    } else if (response.body().getResult().equals("No Record")) {
                        loading.dismiss();
                        Toast.makeText(MoreActivity.this, "No history found", Toast.LENGTH_SHORT).show();
                    } else {
                        loading.dismiss();
                        Toast.makeText(MoreActivity.this, "Out of condition", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    loading.dismiss();
                    Toast.makeText(MoreActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Discover> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(MoreActivity.this, "Unable to reach the server, Please try again later", Toast.LENGTH_SHORT).show();
            }


        });
    }


    private void ShortDesc(String assignment, String week, String mobile) {

        final ProgressDialog loading = ProgressDialog.show(MoreActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        ApiService apiService = RetrofitClient.getApiService();

        Call<Description> call = apiService.Getdescription(assignment, week, mobile);

        call.enqueue(new Callback<Description>() {
            @Override
            public void onResponse(Call<Description> call, Response<Description> response) {

                try {
                    if (response.body().getResult().equals("Success")) {
                        loading.dismiss();

                        short_des.setText(response.body().getData().getShortdesc());
//                        Toast.makeText(UpdateActivity.this, "Something wfghhdghgfhfhchent wrong", Toast.LENGTH_SHORT).show();
//                        UpdateActivity.super.onBackPressed();
                    } else if (response.body().getResult().equals("Error")) {
                        loading.dismiss();
                        Toast.makeText(MoreActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    } else {
                        loading.dismiss();
                        Toast.makeText(MoreActivity.this, "Out of condition", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    loading.dismiss();
                    Toast.makeText(MoreActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Description> call, Throwable t) {
//                                        loading.dismiss();
                Toast.makeText(MoreActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void History(String mobile, String assign) {

        final ProgressDialog loading = ProgressDialog.show(MoreActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

        ApiService apiService = RetrofitClient.getApiService();

        Call<MoreList> call = apiService.Getmoredeatails(mobile, assign);

        call.enqueue(new Callback<MoreList>() {
            @Override
            public void onResponse(Call<MoreList> call, Response<MoreList> response) {

                try {
                    if (response.body().getResult().equals("Success")) {

                        loading.dismiss();
                        recyclerView = (RecyclerView) findViewById(R.id.more_rv);
                        adapter = new MoreAdapter(response.body().getData());
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MoreActivity.this);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);

                    } else if (response.body().getResult().equals("No Record")) {
                        loading.dismiss();
                        Toast.makeText(MoreActivity.this, "No history found", Toast.LENGTH_SHORT).show();
                    } else {
                        loading.dismiss();
                        Toast.makeText(MoreActivity.this, "Out of condition", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    loading.dismiss();
                    Toast.makeText(MoreActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<MoreList> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(MoreActivity.this, "Unable to reach the server, Please try again later", Toast.LENGTH_SHORT).show();
            }


        });
    }
}
