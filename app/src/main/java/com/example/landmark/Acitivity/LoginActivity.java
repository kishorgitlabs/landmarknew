package com.example.landmark.Acitivity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.FragmentManager;

import com.example.landmark.APIService.ApiService;
import com.example.landmark.APIService.RetrofitClient;
import com.example.landmark.Connection.IsInternetConnected;
import com.example.landmark.Model.CoachLocation;
import com.example.landmark.Model.Forgot;
import com.example.landmark.Model.Login;
import com.example.landmark.R;
import com.fivemin.chief.nonetworklibrary.networkBroadcast.NoNet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextView create_account, forgot_password;
    EditText user_name, pass_word;
    AppCompatButton login;
    SharedPreferences myshare, share;
    SharedPreferences.Editor editor, editor1;
    private FragmentManager fm = null;
    private NoNet mNoNet;


    // to check if we are connected to Network
    boolean isConnected = true;

    // to check if we are monitoring Network
    private boolean monitoringConnectivity = false;

//    startService(new Intent(getBaseContext(), MyService.class));

//    public boolean isOnline() {
//        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext()
//                .getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
//
//        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
//            return false;
//        }
//        return true;
//    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        fm = getSupportFragmentManager();
        mNoNet = new NoNet();
        mNoNet.initNoNet(this, fm);


//        if (isOnline()) {
//            // Do you Stuff
//        } else {
//            try {
//                new AlertDialog.Builder(LoginActivity.this)
//                        .setTitle("Error")
//                        .setMessage("Check your internet and try again...")
//                        .setCancelable(false)
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                finish();
//
//                            }
//                        }).show();
//            } catch (Exception e) {
////                Log.d(SyncStateContract.Constants.TAG, "Show Dialog: " + e.getMessage());
//            }
//        }

//        IsInternetConnected.isConnected(LoginActivity.this);

//        isConnected(LoginActivity.this);
        String s = getIntent().getStringExtra("mobile");

        create_account = findViewById(R.id.create_new_account);
        forgot_password = findViewById(R.id.forgot_password);
        user_name = findViewById(R.id.username);
        pass_word = findViewById(R.id.password);
        login = findViewById(R.id.login_button);

        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgetPass();

            }
        });

        create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = user_name.getText().toString();
                String password = pass_word.getText().toString();

                if (username.equals("")) {
                    Toast.makeText(LoginActivity.this, "Please Enter UserName", Toast.LENGTH_SHORT).show();
                } else if (password.equals("")) {
                    Toast.makeText(LoginActivity.this, "Please Enter Password", Toast.LENGTH_SHORT).show();
                } else {


                    final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                    ApiService apiService = RetrofitClient.getApiService();

                    Call<Login> call = apiService.Getapproval(username, password);
                    call.enqueue(new Callback<Login>() {
                        @Override
                        public void onResponse(Call<Login> call, Response<Login> response) {

                            try {
                                if (response.body().getResult().equals("Success")) {
                                    loading.dismiss();
                                    String app = response.body().getData().getMobileno();
                                    myshare = getSharedPreferences("Registration", MODE_PRIVATE);
                                    editor = myshare.edit();
                                    editor.clear();
                                    editor.putString("UserName", user_name.getText().toString());
                                    editor.putString("PassWord", pass_word.getText().toString());
                                    editor.putBoolean("isregister", true);
                                    editor.putString("mob", app);
                                    editor.apply();
                                    editor.commit();
                                    Intent i = new Intent(LoginActivity.this, HomeScreenActivity.class);
//                                i.putExtra("mob",app);
                                    startActivity(i);
                                } else if (response.body().getResult().equals("Not Success")) {
                                    loading.dismiss();
                                    Toast.makeText(LoginActivity.this, "Wait for admin approval", Toast.LENGTH_LONG).show();
                                } else if (response.body().getResult().equals("Error")) {
                                    loading.dismiss();
                                    Toast.makeText(LoginActivity.this, "Username or Password is incorrect", Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
//                            loading.dismiss();
                                e.printStackTrace();
                                Toast.makeText(LoginActivity.this, "gfsdgdfgfdgdfgsdfgrdgrdgdrgdfgdfg", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<Login> call, Throwable t) {
                            loading.dismiss();
                            Toast.makeText(LoginActivity.this, "Unable to reach server,Please try again later", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });
    }

    private void forgetPass() {
        try {
            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LoginActivity.this).create();

            LayoutInflater inflater = ((Activity) LoginActivity.this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.change_password_layout, null);
            alertDialog.setView(dialogView);
            ImageView close = dialogView.findViewById(R.id.close);
            EditText enterMobile = dialogView.findViewById(R.id.new_password);
            TextView update = dialogView.findViewById(R.id.updated_password);
            Button sent = dialogView.findViewById(R.id.change_password_btn);
//            sent.setText("Sent");
//            enterMobile.setHint("Enter Mobile No or Email");

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });

            sent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(sent.getWindowToken(), 0);

//                    try {
//                        InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
//                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//                    } catch (Exception e) {
//                        // TODO: handle exception
//                        e.printStackTrace();
//                    }

                    String EnterMobile = enterMobile.getText().toString();
                    if (EnterMobile.equals("")) {
                        enterMobile.setError("Enter Mobile Number");
                    } else {
                        final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                        ApiService service = RetrofitClient.getApiService();

                        Call<Forgot> call = service.ForgetPassword(EnterMobile);

                        call.enqueue(new Callback<Forgot>() {
                            @Override
                            public void onResponse(Call<Forgot> call, Response<Forgot> response) {
                                loading.dismiss();
                                try {
                                    if (response.body().getResult().equals("Success")) {


                                        android.app.AlertDialog alertDialog1 = new android.app.AlertDialog.Builder(LoginActivity.this).create();

                                        LayoutInflater inflater = ((Activity) LoginActivity.this).getLayoutInflater();
                                        View dialogView = inflater.inflate(R.layout.password_alert, null);
                                        alertDialog1.setView(dialogView);
                                        ImageView close = dialogView.findViewById(R.id.close);
                                        AppCompatButton dis = dialogView.findViewById(R.id.dismiss);
                                        Window window = alertDialog1.getWindow();
                                        window.setLayout(600, 600);
                                        alertDialog1.show();
                                        alertDialog.dismiss();

                                        dis.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alertDialog1.dismiss();
                                            }
                                        });

                                        close.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                alertDialog1.dismiss();
                                            }
                                        });
//                                    enterMobile.setVisibility(View.GONE);
//                                    update.setVisibility(View.VISIBLE);
//                                    update.setText("Password Sent");
//                                    sent.setVisibility(View.GONE);
                                    } else if (response.body().getResult().equals("Error")) {

                                        Toast.makeText(LoginActivity.this, "Please enter registerd Mobile No or Email", Toast.LENGTH_LONG).show();
//                                    enterMobile.setVisibility(View.GONE);
//                                    update.setVisibility(View.VISIBLE);
//                                    update.setText("Please Try Again Later");
//                                    sent.setVisibility(View.GONE);
                                    } else {
                                        Toast.makeText(LoginActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
//                                    enterMobile.setVisibility(View.GONE);
//                                    update.setVisibility(View.VISIBLE);
//                                    update.setText("Please Try Again Later");
//                                    sent.setVisibility(View.GONE);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<Forgot> call, Throwable t) {
                                loading.dismiss();
//                                enterMobile.setVisibility(View.GONE);
//                                update.setVisibility(View.VISIBLE);
////                                update.setText(getString(R.string.server_error));
//                                sent.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            });


            Window window = alertDialog.getWindow();
            window.setLayout(600, 600);
            alertDialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.warning)
                .setTitle("Confirm")
                .setMessage("Are you sure you want to close?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

//    public final static boolean isConnected(Context context) {
//        final ConnectivityManager connectivityManager =
//                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
//        return networkInfo != null && networkInfo.isConnected();
//    }

    @Override
    protected void onResume() {
        mNoNet.RegisterNoNet();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mNoNet.unRegisterNoNet();
        super.onPause();
    }
}
