package com.example.landmark.Acitivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatButton;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.landmark.APIService.ApiService;
import com.example.landmark.APIService.RetrofitClient;
import com.example.landmark.Model.Batch;
import com.example.landmark.Model.CoachLocation;
import com.example.landmark.Model.Datum;
import com.example.landmark.Model.Register;
import com.example.landmark.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {


    EditText phone_no, email, first_name, last_name;

    AppCompatButton register;
    TextView login;
    AutoCompleteTextView location, coach, batch;
    Datum coachLocation;
    private ArrayList<Datum> data;
    private ArrayList<String> names = new ArrayList<String>();
    private ArrayList<String> batchlist = new ArrayList<String>();
    SharedPreferences myshare;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_register);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        phone_no = findViewById(R.id.phone);
        register = findViewById(R.id.register);
        email = findViewById(R.id.email);
        login = findViewById(R.id.login);
        location = findViewById(R.id.location_spinner);
        coach = findViewById(R.id.coach_spinner);
        first_name = findViewById(R.id.first_name);
        last_name = findViewById(R.id.last_name);
        batch = findViewById(R.id.batch_spinner);


//        String phone_check = phone_no.getText().toString();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

//        ApiService apiService = RetrofitClient.getApiService();
//        Call<Batch> call = apiService.BATCH_CALL();
//        call.enqueue(new Callback<Batch>() {
//            @Override
//            public void onResponse(Call<Batch> call, Response<Batch> response) {
//                if (response.body().getResult().equals("Success")) {
//                    try{
//                        for (int i = 0; i < response.body().getData().size(); i++) {
//                            batchlist.add(response.body().getData().get(i).getBatch().toString());
//                        }
//
//                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(RegistrationActivity.this, android.R.layout.simple_dropdown_item_1line, batchlist);
//                        batch.setAdapter(spinnerArrayAdapter);
//                        batch.setCursorVisible(false);
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Batch> call, Throwable t) {
//
//            }
//        });


//        AutoCompleteTextView acTV1 = findViewById(R.id.location_spinner);
        ImageView delButton = findViewById(R.id.location_icon);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.locations));
        final String[] selection = new String[1];
        location.setAdapter(arrayAdapter);
        location.setCursorVisible(false);
        location.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                location.showDropDown();
                selection[0] = (String) parent.getItemAtPosition(position);
//                Toast.makeText(getApplicationContext(), selection[0], Toast.LENGTH_SHORT).show();
                try {

                    String s = location.getText().toString();
                    final ProgressDialog loading = ProgressDialog.show(RegistrationActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                    ApiService apiService = RetrofitClient.getApiService();

                    Call<CoachLocation> call = apiService.GetLocation(s);

                    call.enqueue(new Callback<CoachLocation>() {
                        @Override
                        public void onResponse(Call<CoachLocation> call, Response<CoachLocation> response) {
                            loading.dismiss();

                            if (response.body().getResult().equals("Success")) {

                                try {
                                    for (int i = 0; i < response.body().getData().size(); i++) {
                                        names.add(response.body().getData().get(i).getCoachname().toString());
                                    }
                                    if (names.isEmpty()) {
                                        Toast.makeText(RegistrationActivity.this, "Coach is not available in this location ", Toast.LENGTH_LONG).show();
                                    } else {
                                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(RegistrationActivity.this, android.R.layout.simple_dropdown_item_1line, names);
                                        coach.setAdapter(spinnerArrayAdapter);
                                        coach.setCursorVisible(false);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

//                                String list = response.body().getData().get(position).getCoachname();
//
//                                Toast.makeText(MainActivity.this, list, Toast.LENGTH_LONG).show();
//                                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, Collections.singletonList(list));
//                                final String[] selection = new String[1];
//                                coach.setAdapter(arrayAdapter);
//                                coach.setCursorVisible(false);

//                                coach.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                                    @Override
//                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                                        coach.showDropDown();
//                                        selection[0] = (String) parent.getItemAtPosition(position);
//                                        Toast.makeText(getApplicationContext(), selection[0], Toast.LENGTH_SHORT).show();
//                                    }
//                                });
//                                AutocompleteTextView adapter = new AutocompleteTextView(MainActivity.this, android.R.layout.simple_list_item_1);
//                                coach.setAdapter(adapter);
                            } else if (response.body().getResult().equals("Not Success")) {
                                Toast.makeText(RegistrationActivity.this, "notsucess", Toast.LENGTH_SHORT).show();
                            } else if (response.body().getResult().equals("No Record")) {
                                Toast.makeText(RegistrationActivity.this, "Coach is not available in this location ", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RegistrationActivity.this, "Somthing went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<CoachLocation> call, Throwable t) {
                            loading.dismiss();
                            Toast.makeText(RegistrationActivity.this, "Something went wrong,Please try again later", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                delButton.setAlpha(.8f);
            }
        });

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                location.showDropDown();
                names.clear();
                coach.setText("");
            }
        });

        coach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (location.getText().toString().equals("")) {
                    Toast.makeText(RegistrationActivity.this, "Please select Location", Toast.LENGTH_SHORT).show();
                } else {
                    coach.showDropDown();
                }
            }
        });

        final ProgressDialog dialog = ProgressDialog.show(RegistrationActivity.this, getResources().getString(R.string.app_name), "Loading Batch...", false, false);
        ApiService apiService = RetrofitClient.getApiService();
        Call<Batch> call = apiService.BATCH_CALL();
        call.enqueue(new Callback<Batch>() {
            @Override
            public void onResponse(Call<Batch> call, Response<Batch> response) {
                if (response.body().getResult().equals("Success")) {
                    try {
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            batchlist.add(response.body().getData().get(i).getBatch().toString());
                        }

//                        batchlist.add(response.body().getData().toString());

                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(RegistrationActivity.this, android.R.layout.simple_dropdown_item_1line, batchlist);
                        batch.setAdapter(spinnerArrayAdapter);
                        dialog.dismiss();
                        batch.setCursorVisible(false);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (response.body().getResult().equals("No Record")) {
                    dialog.dismiss();
                    Toast.makeText(RegistrationActivity.this, "No Batch List", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Batch> call, Throwable t) {
                Toast.makeText(RegistrationActivity.this, "Something went wrong,Please try again later", Toast.LENGTH_SHORT).show();
            }
        });

        batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                batch.showDropDown();
//                batchlist.clear();

            }
        });

        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                location.setText(null);
                delButton.setAlpha(.2f);
                selection[0] = null;
                names.clear();
                coach.setText("");
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String first = first_name.getText().toString();
                String last = last_name.getText().toString();
                String mail = email.getText().toString();
                String phone = phone_no.getText().toString();
                String loc = location.getText().toString();
                String coachS = coach.getText().toString();
                String batchS = batch.getText().toString();

                if (first.equals("")) {
                    Toast.makeText(RegistrationActivity.this, "Please enter FirstName", Toast.LENGTH_SHORT).show();
                } else if (last.equals("")) {
                    Toast.makeText(RegistrationActivity.this, "Please enter LastName", Toast.LENGTH_SHORT).show();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
                    Toast.makeText(RegistrationActivity.this, "Please Enter valid E-mail", Toast.LENGTH_SHORT).show();
                } else if (!phone.matches("[0-9]{10}")) {
                    Toast.makeText(RegistrationActivity.this, "Please Enter Valid Phone No", Toast.LENGTH_SHORT).show();
                } else if (loc.equals("")) {
                    Toast.makeText(RegistrationActivity.this, "Please select location", Toast.LENGTH_SHORT).show();
                } else if (coachS.equals("")) {
                    Toast.makeText(RegistrationActivity.this, "Please select coach", Toast.LENGTH_SHORT).show();
                } else if (batchS.equals("")) {
                    Toast.makeText(RegistrationActivity.this, "Please select batch", Toast.LENGTH_SHORT).show();
                } else {

                    final ProgressDialog loading = ProgressDialog.show(RegistrationActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);

                    ApiService apiService = RetrofitClient.getApiService();

                    Call<Register> call = apiService.REGISTER_CALL(first, last, loc, phone, coachS, mail, batchS);
                    call.enqueue(new Callback<Register>() {
                        @Override
                        public void onResponse(Call<Register> call, Response<Register> response) {


                            try {
                                if (response.body().getResult().equals("Success")) {
                                    loading.dismiss();
                                    Toast.makeText(RegistrationActivity.this, "Successfully register, Please wait for Admin approval", Toast.LENGTH_LONG).show();
//
                                    myshare = getSharedPreferences("Mobile", MODE_PRIVATE);
                                    editor = myshare.edit();

                                    editor.putString("mobile", phone);
//                                    editor.putString("PassWord",pass_word.getText().toString());
//                                    editor.putBoolean("isregister",true);
                                    editor.apply();
                                    editor.commit();

                                    Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                                    i.putExtra("mobile", phone);
                                    startActivity(i);
//                                    startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
                                } else if (response.body().getResult().equals("already exist")) {
                                    loading.dismiss();
                                    Toast.makeText(RegistrationActivity.this, "Number already exits", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<Register> call, Throwable t) {
                            loading.dismiss();
                            Toast.makeText(RegistrationActivity.this, "Something went wrong,Please try again later", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }


}