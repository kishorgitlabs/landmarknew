package com.example.landmark.SplashActivity;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;

import com.example.landmark.Acitivity.HomeScreenActivity;
import com.example.landmark.Acitivity.LoginActivity;
import com.example.landmark.R;

public class SplashActivity extends Activity {

    private SharedPreferences myshare;
    private boolean isregister;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        isregister = myshare.getBoolean("isregister", false);
//        editor = myshare.edit();
//        register_check = myshare.getString("UserName", null);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

//                InternetService internetService = new InternetService(SplashActivity.this);
//                internetService.CheckInternet();

                Intent mainIntent;
                if (isregister) {
                    mainIntent = new Intent(SplashActivity.this, HomeScreenActivity.class);
                    finish();
//                  mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                } else {
                    mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    finish();
//                  mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);\
                }
                startActivity(mainIntent);
                finish();
                /* Create an Intent that will start the Menu-Activity. */
            }
        }, 2000);
    }
}
