package com.example.landmark.Connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class InternetService {
    Context context;
    boolean checkingInternet;

    public InternetService(Context context) {
        this.checkingInternet = Boolean.valueOf(false);
        this.context = context;
    }


    public boolean CheckInternet() {
        ConnectivityManager connec = (ConnectivityManager) this.context.getSystemService("connectivity");
        NetworkInfo wifi = connec.getNetworkInfo(1);
        NetworkInfo mobile = connec.getNetworkInfo(0);
        if (wifi.isConnected()) {
            this.checkingInternet = Boolean.valueOf(true);
        } else if (mobile.isConnected()) {
            this.checkingInternet = Boolean.valueOf(true);
        } else {
            this.checkingInternet = Boolean.valueOf(false);
            Toast.makeText(context, "Please check your internet connection ", Toast.LENGTH_SHORT).show();
//            System.exit(0);
        }
        return this.checkingInternet;
    }


}