package com.example.landmark.Connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class IsInternetConnected {

    public final static boolean isConnected(Context context) {
        final ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
