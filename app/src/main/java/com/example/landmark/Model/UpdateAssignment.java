package com.example.landmark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateAssignment {
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private UpdateAssignmentData data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public UpdateAssignmentData getData() {
        return data;
    }

    public void setData(UpdateAssignmentData data) {
        this.data = data;
    }

}