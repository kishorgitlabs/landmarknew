package com.example.landmark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MoreListData {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("mobileno")
    @Expose
    private String mobileno;
    @SerializedName("attendeename")
    @Expose
    private String attendeename;
    @SerializedName("assignment")
    @Expose
    private String assignment;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Completiondate")
    @Expose
    private String completiondate;
    @SerializedName("sourcereason")
    @Expose
    private String sourcereason;
    @SerializedName("insertdate")
    @Expose
    private String insertdate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getAttendeename() {
        return attendeename;
    }

    public void setAttendeename(String attendeename) {
        this.attendeename = attendeename;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompletiondate() {
        return completiondate;
    }

    public void setCompletiondate(String completiondate) {
        this.completiondate = completiondate;
    }

    public String getSourcereason() {
        return sourcereason;
    }

    public void setSourcereason(String sourcereason) {
        this.sourcereason = sourcereason;
    }

    public String getInsertdate() {
        return insertdate;
    }

    public void setInsertdate(String insertdate) {
        this.insertdate = insertdate;
    }

}