package com.example.landmark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Description {
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private DescriptionData data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public DescriptionData getData() {
        return data;
    }

    public void setData(DescriptionData data) {
        this.data = data;
    }

}