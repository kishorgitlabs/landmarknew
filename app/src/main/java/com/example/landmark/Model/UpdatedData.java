package com.example.landmark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdatedData {
    @SerializedName("asc")
    @Expose
    private List<UpdateAsc> asc = null;
    @SerializedName("asc1")
    @Expose
    private List<UpdateAsc1> asc1 = null;

    public List<UpdateAsc> getAsc() {
        return asc;
    }

    public void setAsc(List<UpdateAsc> asc) {
        this.asc = asc;
    }

    public List<UpdateAsc1> getAsc1() {
        return asc1;
    }

    public void setAsc1(List<UpdateAsc1> asc1) {
        this.asc1 = asc1;
    }

}
