package com.example.landmark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeekEnd {
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private List<WeekEndData> data = null;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<WeekEndData> getData() {
        return data;
    }

    public void setData(List<WeekEndData> data) {
        this.data = data;
    }

}