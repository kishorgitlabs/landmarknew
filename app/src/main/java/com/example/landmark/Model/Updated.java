package com.example.landmark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Updated {
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("data")
    @Expose
    private UpdatedData data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public UpdatedData getData() {
        return data;
    }

    public void setData(UpdatedData data) {
        this.data = data;
    }

}