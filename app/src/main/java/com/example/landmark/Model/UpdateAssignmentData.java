package com.example.landmark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateAssignmentData {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("weekend")
    @Expose
    private String weekend;
    @SerializedName("shortdesc")
    @Expose
    private String shortdesc;
    @SerializedName("longdesc")
    @Expose
    private String longdesc;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("assign_id")
    @Expose
    private String assignId;
    @SerializedName("assignment")
    @Expose
    private Object assignment;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("completiondate")
    @Expose
    private Object completiondate;
    @SerializedName("whatwillyoudiscover")
    @Expose
    private Object whatwillyoudiscover;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWeekend() {
        return weekend;
    }

    public void setWeekend(String weekend) {
        this.weekend = weekend;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public void setShortdesc(String shortdesc) {
        this.shortdesc = shortdesc;
    }

    public String getLongdesc() {
        return longdesc;
    }

    public void setLongdesc(String longdesc) {
        this.longdesc = longdesc;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAssignId() {
        return assignId;
    }

    public void setAssignId(String assignId) {
        this.assignId = assignId;
    }

    public Object getAssignment() {
        return assignment;
    }

    public void setAssignment(Object assignment) {
        this.assignment = assignment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getCompletiondate() {
        return completiondate;
    }

    public void setCompletiondate(Object completiondate) {
        this.completiondate = completiondate;
    }

    public Object getWhatwillyoudiscover() {
        return whatwillyoudiscover;
    }

    public void setWhatwillyoudiscover(Object whatwillyoudiscover) {
        this.whatwillyoudiscover = whatwillyoudiscover;
    }

}
