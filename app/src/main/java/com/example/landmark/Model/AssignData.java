package com.example.landmark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AssignData {
    @SerializedName("asc")
    @Expose
    private List<Asc> asc = null;
    @SerializedName("asc1")
    @Expose
    private List<Asc1> asc1 = null;

    public List<Asc> getAsc() {
        return asc;
    }

    public void setAsc(List<Asc> asc) {
        this.asc = asc;
    }

    public List<Asc1> getAsc1() {
        return asc1;
    }

    public void setAsc1(List<Asc1> asc1) {
        this.asc1 = asc1;
    }

}