package com.example.landmark.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.APIService.ApiService;
import com.example.landmark.APIService.RetrofitClient;
import com.example.landmark.Acitivity.AssignmentListActivity;
import com.example.landmark.Acitivity.ClassroomActivity;
import com.example.landmark.Adapter.AssignmentListAdapter;
import com.example.landmark.Adapter.DashboardAdapter;
import com.example.landmark.Model.AssignList;
import com.example.landmark.Model.BatchData;
import com.example.landmark.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class DashboardFragment extends Fragment {

    LinearLayout complete_assign, classroom;
    RecyclerView rv;
    AssignmentListAdapter adapter;
    DashboardAdapter adapter1;
    ConstraintLayout constraintLayout;
    private ArrayList<String> names = new ArrayList<String>();
    List<BatchData> data;
    SharedPreferences share;

    @Override
    public void onResume() {
        super.onResume();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_fragment, container, false);
        complete_assign = view.findViewById(R.id.completed_assignment);
        classroom = view.findViewById(R.id.classroom_layout);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


//        LinearLayout linearLayout = view.findViewById(R.id.anime);
//        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f,1.0f);
//        alphaAnimation.setFillAfter(true);
//        alphaAnimation.setDuration(1200);
//        linearLayout.startAnimation(alphaAnimation);

        ASP(view);
//        ASP1(view);

        complete_assign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AssignmentListActivity.class);
//                startActivity(i);
                Activity activity = (Activity) getActivity();
                activity.startActivity(i);
                activity.overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });

        classroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ClassroomActivity.class);
//                startActivity(i);
                Activity activity = (Activity) getActivity();
                activity.startActivity(i);
                activity.overridePendingTransition(R.anim.left_in, R.anim.left_out);

            }
        });


        return view;
    }

//    private void ASP1(View view) {
//
//        final ProgressDialog loading1 = ProgressDialog.show(getActivity(), getResources().getString(R.string.app_name), "Please wait...", false, false);
//
//        ApiService apiService1 = RetrofitClient.getApiService();
//
//        Call<Delete1> call1 = apiService1.GetCompleted();
//
//        call1.enqueue(new Callback<Delete1>() {
//            @Override
//            public void onResponse(Call<Delete1> call, Response<Delete1> response1) {
//
//                try {
//                    if (response1.body().getResult().equals("Success")) {
//
//
//                        loading1.dismiss();
////                        rv1 = view.findViewById(R.id.rvassign1);
//                        adapter1 = new DashboardAdapter(response1.body().getData());
//                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
//                        rv1.setLayoutManager(layoutManager);
//                        rv1.setAdapter(adapter1);
//
//                    } else {
//                        loading1.dismiss();
//                        Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
//                    }
//
//                } catch (Exception e) {
//                    loading1.dismiss();
//
//                    Toast.makeText(getContext(), "Internet connection error \uD83D\uDE1E", Toast.LENGTH_SHORT).show();
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Delete1> call, Throwable t) {
//                loading1.dismiss();
//                new AlertDialog.Builder(getContext())
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .setTitle("Confirm")
//                        .setMessage("Please check your internet connection and try again")
//                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                getActivity().finish();
//                                System.exit(0);
////                                    Toast.makeText(getActivity(), "Successfully Loged out", Toast.LENGTH_SHORT).show();
//                            }
//
//                        })
////                        .setNegativeButton("No", null)
//                        .show();
//                Toast.makeText(getActivity(), "Internet connection error \uD83D\uDE1E", Toast.LENGTH_LONG).show();
//            }
//        });
//    }

    private void ASP(View view) {

        share = this.getActivity().getSharedPreferences("Registration", MODE_PRIVATE);

        String mob = share.getString("mob", "");

        final ProgressDialog loading = ProgressDialog.show(getActivity(), getResources().getString(R.string.app_name), "Please wait...", false, false);

        ApiService apiService = RetrofitClient.getApiService();

        Call<AssignList> call = apiService.ASSIGN_LIST_CALL(mob);

        call.enqueue(new Callback<AssignList>() {
            @Override
            public void onResponse(Call<AssignList> call, Response<AssignList> response) {

                try {
                    if (response.body().getResult().equals("Success")) {


                        loading.dismiss();
                        rv = view.findViewById(R.id.rvassign);

                        adapter = new AssignmentListAdapter(getActivity(), response.body().getData().getAsc(), response.body().getData().getAsc1());
//                        adapter = new AssignmentListAdapter(response.body().getData());
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(adapter);

                    } else {
                        loading.dismiss();
                        Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    loading.dismiss();
                    Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AssignList> call, Throwable t) {
                loading.dismiss();
//                Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();
//                Log.e("dfsfsdfdsf", t.getMessage());
            }
        });
    }
}
