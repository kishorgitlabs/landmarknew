package com.example.landmark.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.Acitivity.AssignmentListActivity;
import com.example.landmark.Model.Asc;
import com.example.landmark.Model.Asc1;
import com.example.landmark.Model.DeleteData;
import com.example.landmark.R;

import java.util.List;

public class AssignmentListAdapter extends RecyclerView.Adapter<AssignmentListAdapter.ViewHolder> {

    private final List<Asc> list;
    private final List<Asc1> datalist;
    //    private final List<Delete1Data> list;
    Context context;


    public AssignmentListAdapter(Context context, List<Asc> list, List<Asc1> datalist) {
        this.context = context;
        this.list = list;
        this.datalist = datalist;


    }

    @NonNull
    @Override
    public AssignmentListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.assign_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssignmentListAdapter.ViewHolder holder, int position) {


//        holder.txtcount.setText(datalist.get(position).getCount());
        holder.txtweek.setText(list.get(position).getWeekend());
        holder.txtassign.setText(list.get(position).getCount());


        for (int i = 0; i < datalist.size(); i++) {

            if (list.get(position).getWeekend().equalsIgnoreCase(datalist.get(i).getWeekend())) {
                holder.txtcount.setText(datalist.get(i).getCount());
                break;
            } else {
                holder.txtcount.setText("0");
            }
        }

//        for (int i = 0; i < dataList.size(); i++) {
//            if (i < list.size()) {
//                holder.txtcount.setText(list.get(i).getCount());
//            }else {
//                holder.txtcount.setText("0");
//            }
//        }

//            if (i < list.size()) {
//                holder.txtcount.setText(list.get(i).getCount());
//            } else if (i > list.size()) {
//                holder.txtcount.setText("0");
//            }
//            else {
//                holder.txtcount.setText(list.get(position).getCount());
//            }


//        List<String> lis1t = new ArrayList<>();
//        for(int i = 0 ; i<list.size(); i++) {
//            lis1t.add(list.get(i).getCount());
//            holder.txtcount.setText(lis1t.toString());
////            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, lis1t);
////            holder.txtcount.setText(arrayAdapter.toString());
//        }


    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        } else {
            return datalist.size();
        }


    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtweek, txtcount, txtassign;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtweek = (TextView) itemView.findViewById(R.id.assign_weekend);
            txtcount = (TextView) itemView.findViewById(R.id.assign_count);
            txtassign = (TextView) itemView.findViewById(R.id.total);

            txtweek.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), AssignmentListActivity.class);
                    v.getContext().startActivity(intent);
                }
            });

            txtcount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), AssignmentListActivity.class);
                    v.getContext().startActivity(intent);
                }
            });

            txtassign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), AssignmentListActivity.class);
                    v.getContext().startActivity(intent);
                }
            });


        }
    }
}
