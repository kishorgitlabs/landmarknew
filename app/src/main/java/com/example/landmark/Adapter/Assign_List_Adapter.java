package com.example.landmark.Adapter;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.Acitivity.DescriptionActivity;
import com.example.landmark.Acitivity.UpdateActivity;
import com.example.landmark.Model.WeekEndData;
import com.example.landmark.R;

import java.util.List;

public class Assign_List_Adapter extends RecyclerView.Adapter<Assign_List_Adapter.ViewHolder> {

    private List<WeekEndData> dataList;


    public Assign_List_Adapter(List<WeekEndData> list) {
        this.dataList = list;

    }

    @NonNull
    @Override
    public Assign_List_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.assign_list_adapter, parent, false);
        return new Assign_List_Adapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Assign_List_Adapter.ViewHolder holder, int position) {


        holder.txt1.setText(dataList.get(position).getAssignId());

        String[] date = dataList.get(position).getCompletiondate().split("T");

        holder.txt2.setText(dataList.get(position).getCompletiondate());

        if (dataList.get(position).getCompletiondate().isEmpty()) {
            holder.txt2.setText("N/A");
        } else {
//            holder.txt2.setText(dataList.get(position).getCompletiondate());
            holder.txt2.setText(date[0]);
        }

        if (dataList.get(position).getStatus().isEmpty()) {
            holder.txt3.setText("N/A");
        } else if (dataList.get(position).getStatus().equals("Pending")) {
            holder.txt3.setTextColor(Color.parseColor("#db5860"));
            holder.txt2.setTextColor(Color.parseColor("#db5860"));
            holder.txt1.setTextColor(Color.parseColor("#db5860"));
            holder.txt3.setText(dataList.get(position).getStatus());
        } else if (dataList.get(position).getStatus().equals("Completed")) {
            holder.txt3.setTextColor(Color.parseColor("#ffcd42"));
            holder.txt2.setTextColor(Color.parseColor("#188eec"));
            holder.txt1.setTextColor(Color.parseColor("#188eec"));
            holder.button.setVisibility(View.GONE);
            holder.txt3.setText(dataList.get(position).getStatus());

        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txt1, txt2, txt3;
        AppCompatButton button;
        String s1;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt1 = (TextView) itemView.findViewById(R.id.txt1);
            txt2 = (TextView) itemView.findViewById(R.id.txt2);
            txt3 = (TextView) itemView.findViewById(R.id.txt3);
            button = (AppCompatButton) itemView.findViewById(R.id.rvbutton);

            button.setOnClickListener(this);
            txt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String s = txt1.getText().toString();
                    Intent i = new Intent(v.getContext(), DescriptionActivity.class);
                    i.putExtra("value", s);
                    v.getContext().startActivity(i);
                }
            });

        }


        @Override
        public void onClick(View v) {
//            Toast.makeText(v.getContext(), "ITEM PRESSED = gdfgsdgdrfgfdgdgdf" + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            String s = txt1.getText().toString();
            Intent i = new Intent(v.getContext(), UpdateActivity.class);
            i.putExtra("value", s);
            v.getContext().startActivity(i);
        }
    }


}

