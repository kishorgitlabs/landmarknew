package com.example.landmark.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.Acitivity.AssignmentListActivity;
import com.example.landmark.Model.Delete1Data;
import com.example.landmark.R;

import java.util.List;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    private final List<Delete1Data> dataList;

    public DashboardAdapter(List<Delete1Data> datalist) {
        this.dataList = datalist;
    }

    @NonNull
    @Override
    public DashboardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.dashboardadapter, parent, false);
        return new DashboardAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardAdapter.ViewHolder holder, int position) {

        holder.textView.setText(dataList.get(position).getCount());

//        for(int i =0;  i<dataList.size(); i++){
//            if(i<dataList.size()) {
//                holder.textView.setText(dataList.get(i).getCount());
//            }else {
//                holder.textView.setText("0");
//            }
//        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txt);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), AssignmentListActivity.class);
                    v.getContext().startActivity(intent);
                }
            });
        }
    }
}
