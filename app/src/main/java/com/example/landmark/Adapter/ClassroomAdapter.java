package com.example.landmark.Adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.Model.ClassData;
import com.example.landmark.R;

import java.util.ArrayList;
import java.util.List;

public class ClassroomAdapter extends RecyclerView.Adapter<ClassroomAdapter.ViewHolder> {

    private List<ClassData> dataList;

    public ClassroomAdapter(List<ClassData> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ClassroomAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.classroom_adapter, parent, false);
        return new ClassroomAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassroomAdapter.ViewHolder holder, int position) {
        holder.txtid.setText(String.valueOf(dataList.get(position).getId()));
        holder.txtshedule.setText(dataList.get(position).getSchedule());

        String con = "Week";

        if (dataList.get(position).getWeekend().contains(con)) {
            holder.txtclass.setTextColor(Color.parseColor("#f42943"));
            holder.txtid.setTextColor(Color.parseColor("#f42943"));
            holder.txtshedule.setTextColor(Color.parseColor("#f42943"));

        } else if (dataList.get(position).getWeekend().contains("Class")) {
            holder.txtclass.setTextColor(Color.parseColor("#000000"));
            holder.txtid.setTextColor(Color.parseColor("#000000"));
            holder.txtshedule.setTextColor(Color.parseColor("#000000"));
        }
        holder.txtclass.setText(dataList.get(position).getWeekend());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtid, txtclass, txtshedule;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtid = (TextView) itemView.findViewById(R.id.id);
            txtclass = (TextView) itemView.findViewById(R.id.classadpter);
            txtshedule = (TextView) itemView.findViewById(R.id.shedule);
        }
    }
}
