package com.example.landmark.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.Acitivity.DescriptionActivity;
import com.example.landmark.Acitivity.MoreActivity;
import com.example.landmark.Acitivity.UpdateActivity;
import com.example.landmark.Model.AssignmentData;
import com.example.landmark.Model.WeekendPendingData;
import com.example.landmark.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Seperate_Filter_Adapter extends RecyclerView.Adapter<Seperate_Filter_Adapter.ViewHolder> {

    List<WeekendPendingData> datalist;
    String list1;
    Context context;

    public Seperate_Filter_Adapter(List<WeekendPendingData> datalist, Context context) {
        this.datalist = datalist;
        this.context = context;
    }

    @NonNull
    @Override
    public Seperate_Filter_Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.assign_list_adapter1, parent, false);
        return new Seperate_Filter_Adapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Seperate_Filter_Adapter.ViewHolder holder, int position) {

        holder.txt1.setText(datalist.get(position).getAssignId());

        String[] date = datalist.get(position).getCompletiondate().split("T");

        holder.txt2.setText(datalist.get(position).getCompletiondate());

        if (datalist.get(position).getCompletiondate().isEmpty()) {
            holder.txt2.setText("N/A");
        } else {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            String inputDateStr = date[0];
            Date date1 = null;
            try {
                date1 = inputFormat.parse(inputDateStr);
                String outputDateStr = outputFormat.format(date1);
                holder.txt2.setText(outputDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }


        }

        if (datalist.get(position).getStatus().isEmpty()) {
            holder.txt3.setText("N/A");
        } else if (datalist.get(position).getStatus().equals("Pending")) {
            holder.txt3.setTextColor(Color.parseColor("#db5860"));
            holder.txt2.setTextColor(Color.parseColor("#db5860"));
//            holder.txt1.setTextColor(Color.parseColor("#db5860"));
            holder.txt3.setText(datalist.get(position).getStatus());
        } else if (datalist.get(position).getStatus().equals("Completed")) {
            holder.txt3.setTextColor(Color.parseColor("#008140"));
            holder.txt2.setTextColor(Color.parseColor("#008140"));
//            holder.txt1.setTextColor(Color.parseColor("#008140"));
            holder.butto.setVisibility(View.GONE);
            holder.more.setVisibility(View.VISIBLE);
            holder.txt3.setText(datalist.get(position).getStatus());
        } else if (datalist.get(position).getStatus().equals("Inprogress")) {
            holder.txt3.setText(datalist.get(position).getStatus());
        }
    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt1, txt2, txt3;
        AppCompatButton butto;
        TextView more;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt1 = (TextView) itemView.findViewById(R.id.txt1);
            txt2 = (TextView) itemView.findViewById(R.id.txt2);
            txt3 = (TextView) itemView.findViewById(R.id.txt3);
            butto = (AppCompatButton) itemView.findViewById(R.id.rvbutton);
            more = itemView.findViewById(R.id.more_button);

//            more = itemView.findViewById(R.id.more_button);

            butto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String sta = txt3.getText().toString();

                    SharedPreferences sharedPreferences1 = context.getSharedPreferences("Adap", Context.MODE_PRIVATE);

                    String s = sharedPreferences1.getString("Ok_200", "");
                    Intent i = new Intent(v.getContext(), UpdateActivity.class);


                    //-----> s is assignment name from shared preferences
                    i.putExtra("value", s);
                    i.putExtra("sta", sta);
                    v.getContext().startActivity(i);


                }
            });

            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    String s = txt1.getText().toString();
                    String c = txt3.getText().toString();
                    Intent intent = new Intent(v.getContext(), MoreActivity.class);

                    //-----> s is assignment name from shared preferences
//                    intent.putExtra("value", s);
                    intent.putExtra("status", c);
                    v.getContext().startActivity(intent);
                }
            });

//            txt1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    String s = txt1.getText().toString();
//
//                    Intent i = new Intent(v.getContext(), DescriptionActivity.class);
//                    i.putExtra("value", s);
//                    v.getContext().startActivity(i);
//                }
//            });

        }


//        @Override
//        public void onClick(View v) {
////            String assign = txt1.getText().toString();
//            String sta = txt3.getText().toString();
//            Intent i = new Intent(v.getContext(), UpdateActivity.class);
//            i.putExtra("value", s);
//            i.putExtra("sta",sta);
//            v.getContext().startActivity(i);
//        }
    }


}

