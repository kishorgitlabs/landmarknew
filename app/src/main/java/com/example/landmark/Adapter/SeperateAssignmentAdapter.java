package com.example.landmark.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.Acitivity.DescriptionActivity;
import com.example.landmark.Acitivity.MoreActivity;
import com.example.landmark.Acitivity.UpdateActivity;
import com.example.landmark.Model.UpdateAsc;
import com.example.landmark.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SeperateAssignmentAdapter extends RecyclerView.Adapter<SeperateAssignmentAdapter.Viewholder> {

    private List<UpdateAsc> list;
//    private List<UpdateAsc1> dataList;
    Context context;

    public SeperateAssignmentAdapter(Context context, List<UpdateAsc> list) {
        this.context = context;
        this.list = list;


    }

    @NonNull
    @Override
    public SeperateAssignmentAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.updated_adapter, parent, false);
        return new SeperateAssignmentAdapter.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SeperateAssignmentAdapter.Viewholder holder, int position) {

        holder.txt1.setText(list.get(position).getAssignId());

        String[] date = list.get(position).getCompletiondate().split("T");

        if (list.get(position).getCompletiondate().isEmpty()) {
            holder.txt2.setText("N/A");
        } else {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            String inputDateStr = date[0];
            Date date1 = null;
            try {
                date1 = inputFormat.parse(inputDateStr);
                String outputDateStr = outputFormat.format(date1);
                holder.txt2.setText(outputDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }



        if(list.get(position).getStatus().equalsIgnoreCase("Pending")){
            holder.txt3.setTextColor(Color.parseColor("#db5860"));
            holder.txt2.setTextColor(Color.parseColor("#db5860"));
//            holder.txt2.setText(date[0]);
            holder.txt3.setText(list.get(position).getStatus());
        }else if(list.get(position).getStatus().equalsIgnoreCase("Completed")){
            holder.txt3.setTextColor(Color.parseColor("#008140"));
            holder.txt2.setTextColor(Color.parseColor("#008140"));
//            holder.txt2.setText(date[0]);
            holder.txt3.setText(list.get(position).getStatus());
        }
//        holder.txt2.setText(list.get(position).getCompletiondate());
//        holder.txt3.setText(list.get(position).getStatus());




//        for (int i = 0; i < list.size(); i++) {
//            if (dataList.get(position).getAssignId().equalsIgnoreCase(list.get(i).getAssignId())) {
//
//                String[] date = list.get(i).getCompletiondate().split("T");
//
//                if(list.get(i).getCompletiondate().equals(null)){
//                    holder.txt2.setText("N/A");
//                }else {
//                    holder.txt2.setText(date[0]);
//                }
//
//                if(list.get(i).getStatus().equalsIgnoreCase("Pending")) {
//                    holder.txt3.setTextColor(Color.parseColor("#db5860"));
//                    holder.txt2.setTextColor(Color.parseColor("#db5860"));
//                    holder.txt2.setText(date[0]);
//                    holder.txt3.setText(list.get(i).getStatus());
//                }else if(list.get(i).getStatus().equalsIgnoreCase("Inprogress")){
//                    holder.txt2.setText(date[0]);
//                    holder.txt3.setText(list.get(i).getStatus());
//                } else if(list.get(i).getStatus().equalsIgnoreCase("Completed")){
//                    holder.txt3.setTextColor(Color.parseColor("#008140"));
//                    holder.txt2.setTextColor(Color.parseColor("#008140"));
//                    holder.txt2.setText(date[0]);
//                    holder.txt3.setText(list.get(i).getStatus());
//                    holder.butto.setVisibility(View.GONE);
//                    holder.more.setVisibility(View.VISIBLE);
//                }
//
//                break;
//            } else {
//                holder.txt2.setText("N/A");
//                holder.txt3.setText("N/A");
//            }
//
//        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView txt1, txt2, txt3;
        AppCompatButton butto;
        TextView more;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            txt1 = (TextView) itemView.findViewById(R.id.txt1);
            txt2 = (TextView) itemView.findViewById(R.id.txt2);
            txt3 = (TextView) itemView.findViewById(R.id.txt3);
            butto = (AppCompatButton) itemView.findViewById(R.id.rvbutton);
            more = itemView.findViewById(R.id.more_button);

            butto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), UpdateActivity.class);
                    i.putExtra("HHHH", txt1.getText().toString());
                    v.getContext().startActivity(i);
                }
            });

            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), MoreActivity.class);
                    i.putExtra("HHHH", txt1.getText().toString());
                    v.getContext().startActivity(i);
                }
            });

            txt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), DescriptionActivity.class);
                    i.putExtra("HHHH", txt1.getText().toString());
                    v.getContext().startActivity(i);
                }
            });
        }
    }
}
