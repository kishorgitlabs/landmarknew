package com.example.landmark.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.landmark.Model.MoreListData;
import com.example.landmark.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MoreAdapter extends RecyclerView.Adapter<MoreAdapter.ViewHolder> {

    List<MoreListData> listData;

    public MoreAdapter(List<MoreListData> data) {
        this.listData = data;
    }

    @NonNull
    @Override
    public MoreAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.more_adapter, parent, false);
        return new MoreAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoreAdapter.ViewHolder holder, int position) {

        String[] date = listData.get(position).getInsertdate().split("T");
//            holder.txtdate.setText(date[0]);

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
        String inputDateStr = date[0];
        Date date1 = null;
        try {
            date1 = inputFormat.parse(inputDateStr);
            String outputDateStr = outputFormat.format(date1);
            holder.txtdate.setText(outputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.txtstatus.setText(listData.get(position).getStatus());
//        holder.txtdate.setText(listData.get(position).getSourcereason());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtdate, txtstatus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtdate = itemView.findViewById(R.id.date_);
            txtstatus = itemView.findViewById(R.id.status_);
        }
    }
}
