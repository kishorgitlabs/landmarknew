package com.example.landmark.APIService;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    public static final String ROOT_URL = "http://brainmagicllc.com/landmarkjson/";

    /* private static Retrofit getRetrofitInstance() {
         return new Retrofit.Builder()
                 .baseUrl(ROOT_URL)
                 .addConverterFactory(GsonConverterFactory.create())
                 .build();
     }*/
    private static Retrofit getRetrofitInstance() {
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(new GsonBuilder()
                .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    private static Retrofit getRetrofitQualityInstance() {
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(new GsonBuilder()
                .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                .create());

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.MINUTES)
                .writeTimeout(15, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.MINUTES)
                .build();
    }


    /**
     * Get API Service
     *
     * @return API Service
     */
    public static ApiService getApiService() {
        return getRetrofitInstance().create(ApiService.class);
    }

    public static ApiService getApiQualityService() {
        return getRetrofitQualityInstance().create(ApiService.class);
    }
}
