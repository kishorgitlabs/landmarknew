package com.example.landmark.APIService;

import com.example.landmark.Model.AssignList;
import com.example.landmark.Model.Assignment;
import com.example.landmark.Model.Batch;
import com.example.landmark.Model.Classroom;
import com.example.landmark.Model.CoachLocation;
import com.example.landmark.Model.Delete1;
import com.example.landmark.Model.Description;
import com.example.landmark.Model.Discover;
import com.example.landmark.Model.Forgot;
import com.example.landmark.Model.Login;
import com.example.landmark.Model.MoreList;
import com.example.landmark.Model.Pending;
import com.example.landmark.Model.Register;
import com.example.landmark.Model.SourceList;
import com.example.landmark.Model.UpdateAssignment;
import com.example.landmark.Model.Updated;
import com.example.landmark.Model.WeekEnd;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {

    @FormUrlEncoded
    @POST("api/Values/retrievedata")
    public Call<CoachLocation> GetLocation(
            @Field("location") String location);

    @FormUrlEncoded
    @POST("api/Values/register")
    public Call<Register> REGISTER_CALL(
            @Field("Firstname") String Firstname,
            @Field("Lastname") String Lastname,
            @Field("Location") String Location,
            @Field("Mobileno") String MobileNo,
            @Field("Coach") String Coach,
            @Field("Email") String Email,
            @Field("Batch") String Batch);


    @GET("api/Values/getbatch")
    public Call<Batch> BATCH_CALL();

    @GET("api/Values/getweek")
    public Call<Classroom> CLASS_DATA_CALL();

    @FormUrlEncoded
    @POST("api/Values/getcount")
    public Call<AssignList> ASSIGN_LIST_CALL(
            @Field("mobileno") String mobileno
    );

    @GET("api/Values/getcountcompleted")
    public Call<Delete1> GetCompleted();

    @FormUrlEncoded
    @POST("api/Values/Approve")
    public Call<Login> Getapproval(
            @Field("username") String mobileno,
            @Field("password") String pass);

    @FormUrlEncoded
    @POST("api/Values/getweekend")
    public Call<WeekEnd> Getweekend(
            @Field("weekend") String weekend);

    @FormUrlEncoded
    @POST("api/Values/getweekendpending")
    public Call<Updated> Getweekendpending(
            @Field("weekend") String weekend,
            @Field("Status") String status,
            @Field("mobileno") String mob);

    @FormUrlEncoded
    @POST("api/Values/getdetails")
    public Call<Description> Getdescription(
            @Field("assignment") String assign,
            @Field("weekend") String week,
            @Field("mobileno") String mob);

    @FormUrlEncoded
    @POST("api/Values/getmoredetails")
    public Call<MoreList> Getmoredeatails(
            @Field("mobileno") String mob,
            @Field("assignment") String assign);

    @FormUrlEncoded
    @POST("api/Values/forgotpassword")
    public Call<Forgot> ForgetPassword(
            @Field("mobileno") String mob);


    @FormUrlEncoded
    @POST("api/Values/getmoredetailsnew")
    public Call<Discover> GetDiscover(
            @Field("mobileno") String mob,
            @Field("assignment") String assign,
            @Field("status") String status);

    @FormUrlEncoded
    @POST("api/Values/updateassignment")
    public Call<UpdateAssignment> GetUpdateAssign(
            @Field("weekend") String week,
            @Field("assignment") String assign,
            @Field("Status") String status,
            @Field("completiondate") String completedata,
            @Field("mobileno") String mobile,
            @Field("whatwillyoudiscover") String what,
            @Field("sourcereason") String reason);

    @GET("api/Values/getsource")
    public Call<SourceList> Getsource();

    @FormUrlEncoded
    @POST("api/Values/getassignmentlist")
    public Call<Assignment> GetAssignmentList(
            @Field("weekend") String week);
}
